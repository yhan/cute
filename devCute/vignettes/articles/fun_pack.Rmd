---
title: "fun_pack"
---
<br>

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(devCute)
```
<br>

### Datasets
```{r}
reqpkg <- "ggplot2"
grouppkg <- c("ggplot2", "usethis")
```
<br>

### Datasets info
```{r}
# reqpkg # character vector of package names
# grouppkg # character vector of package names
```
<br>

### Simple example
```{r}
fun_pack(req.package = reqpkg)
fun_pack(req.package = grouppkg)
```
<br>

### Argument load
```{r}
fun_pack(req.package = grouppkg, load = TRUE) # load the group of package in the environment 
```
<br>

### Argument lib.path
```{r}
fun_pack(req.package = grouppkg, lib.path = "C:/Users/yhan/AppData/Local/R/win-library/4.3") # require the package contained in the absolute pathways of the directories
```
<br>

### All the arguments
```{r}
fun_pack(
    req.package = reqpkg, 
    load = TRUE,
    lib.path = "C:/Users/yhan/AppData/Local/R/win-library/4.3"
) # require the packages in the absolute pathways of the directories and load them in the environment
```